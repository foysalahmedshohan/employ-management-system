<!DOCTYPE html>
<!--
Template Name: Escarine-Hol
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html>
<head>
<title>Fair-bond Ltd.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<link href="../layout/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Top Background Image Wrapper -->






  <!-- ################################################################################################ -->
  <div class="wrapper row0" style="border-bottom: 1px solid black;">
    <div id="topbar" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <div class="fl_left">
        <ul class="nospace inline pushright">
          <li><i class="fa fa-phone" style="margin-left:-150px; " ></i> +880-2-9134502</li>
          <li><i class="fa fa-envelope-o"style="margin-left:390px; "></i> hamid@fair-bond.com</li>
        </ul>
      </div>
      <div class="fl_right" style="margin-right:-150px; ">
        <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
          <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
          <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a class="faicon-rss" href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
      </div>
      <!-- ################################################################################################ -->
    </div>
  </div>
  <!-- ################################################################################################ -->






  <!-- ################################################################################################ -->
  <!-- ################################################################################################ -->
  <div class="wrapper row1">
    <header id="header1" class="hoc clear"> 
      <!-- ################################################################################################ -->
      <div id="logo" class="">

        <div class="inline">
          <img src="../images/demo/logo2.png" style="height: 20px;margin-left:350px; margin-top: -13px;   width: 35px;" alt=""></a>

        <h2><a href="index.html" style=" color: white;">FAIR-BOND LIMITED</a></h2>

        </div>

      </div>
      
      <!-- ################################################################################################ -->
    </header>
  
  
  <header id="header" class="hoc clear" style="border-bottom: 1px solid black; margin-top: 5px;"> 
      <!-- ################################################################################################ -->
      <nav id="mainav" class="">
        <ul class="clear">
          <i class="fa fa-home"></i>
          <li class="active "><a href="../index.php">Home</a></li>
      <li><a href="cp.php">Company Profile</a></li>
              <li><a href="hc.php">Hexagon Camicals</a></li>
         <li><a class="drop" href="#">Products</a>
            <ul style="margin-top:-17px;">
              <li><a href="pigment.php">Pigment</a></li>
              <li><a href="resins.php">Resins</a></li>             
              <li><a href="addi.php">Aditives/Bioside</a></li>
              <li><a href="petro.php">Petrocamical/Monomals</a></li>             
              <li><a href="filter.php">Filters Minerels Salt</a></li>
            </ul>
          </li>
              <li><a href="global.php">Global Partner</a></li>
              <li><a href="client.php">Our Clients</a></li>
        <li><a href="gallery.php">Gallery</a></li>
          <li><a href="news.php">Latest News</a></li>
          <li><a href="contact.php">Contact Us</a></li>
         
        </ul>
      </nav>
      <!-- ################################################################################################ -->
    </header>
  
  
  </div>
  <!-- ################################################################################################ -->
  <!-- ### background: url(images/demo/4.jpg); ############################################################################################# -->
