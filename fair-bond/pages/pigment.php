<?php 
include_once "../header.php";

?>

      <div class="banner" >
        <img src="../new/images.jpg" style="height:100px; width: 100%;" alt="">

      </div>
       
      	

	<div class="container" style="text-align: center;min-height: 100px; text-align: justify;">
		<h3>Pigment</h3>
		<hr>
		<p>Pigments are insoluble and are applied not as solutions but as finely ground solid particles mixed with a liquid. In general, the same pigments are employed in oil- and water-based paints, printing inks, and plastics. Pigments may be organic (i.e., contain carbon) or inorganic. The majority of inorganic pigments are brighter and last longer than organic ones. Organic pigments made from natural sources have been used for centuries, but most pigments used today are either inorganic or synthetic organic ones. Synthetic organic pigments are derived from coal tars and other petrochemicals. Inorganic pigments are made by relatively simple chemical reactions—notably oxidation—or are found naturally as earths. </p>
  <div class="row">
    <div class="col-sm-4">
	      <img src="../New/pigment/1.jpg">

	   
    </div>
    <div class="col-sm-4">
	     <img src="../New/pigment/2.jpg">
	   
    </div>
    <div class="col-sm-4">
	      <img src="../New/pigment/3d-illustration-molecule-model-science-medical_shutterstock_612196760.jpg">
	     
    </div>
  </div>
</div>



    


<?php 
include_once "../footer.php";

?>