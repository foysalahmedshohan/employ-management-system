<!DOCTYPE html>
<!--
Template Name: Escarine-Hol
Author: <a href="https://www.os-templates.com/">OS Templates</a>
Author URI: https://www.os-templates.com/
Licence: Free to use under our free template licence terms
Licence URI: https://www.os-templates.com/template-terms
-->
<html>
<head>
<title>Fair-bond Ltd.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
<link href="layout/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
</head>

<link href="layout/css/scss/grid.scss" rel="stylesheet" type="text/css" media="all">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">


</head>
<body id="top">
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- Top Background Image Wrapper -->






  <!-- ################################################################################################ -->
  <div class="all-header">
  <div class="wrapper row0" style="border-bottom: 1px solid black;">
    <div id="topbar" class="hoc clear"> 

      <div class="fl_left">
        <ul class="nospace inline pushright">
          <li><i class="fa fa-phone" style="margin-left:-150px; " ></i> +880-2-9134502</li>
          <li><i class="fa fa-envelope-o"style="margin-left:410px; "></i> hamid@fair-bond.com</li>
        </ul>
      </div>
      <div class="fl_right" style="margin-right:-150px; ">
        <ul class="faico clear">
          <li><a class="faicon-facebook" href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-pinterest" href="#"><i class="fa fa-pinterest"></i></a></li>
          <li><a class="faicon-twitter" href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a class="faicon-dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
          <li><a class="faicon-google-plus" href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a class="faicon-rss" href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
      </div>
   
    </div>
  </div>

  <div class="wrapper row1">
    <div id="header1" class="hoc clear"> 

      <div id="logo" class="">

	      <div class="inline">
          <a href="index.html"><img src="images/demo/logo2.png" style="height: 20px;margin-left:350px; margin-top: -13px;   width: 35px;" alt=""></a>

        <h2><a href="index.html" style=" color: white;">FAIR-BOND LIMITED</a></h2>

        </div>

      </div>
      

    </div>
	
	
	<div id="header" class="hoc clear" style="border-bottom: 1px solid black; margin-top: 5px;"> 

      <nav id="mainav" class="">
        <ul class="clear">
          <i class="fa fa-home"></i>
          <li class="active1 "><a href="index.php">Home</a></li>
      <li><a href="pages/cp.php">Company Profile</a></li>
              <li><a href="pages/hc.php">Hexagon Camicals</a></li>
         <li><a class="drop" href="#">Products</a>
            <ul style="margin-top:-17px;">
              <li><a href="pages/pigment.php">Pigment</a></li>
              <li><a href="pages/resins.php">Resins</a></li>             
              <li><a href="pages/addi.php">Aditives/Bioside</a></li>
              <li><a href="pages/petro.php">Petrocamicals/Monomal</a></li>             
              <li><a href="pages/filter.php">Filters Minerels Salt</a></li>
            </ul>
          </li>
              <li><a href="pages/global.php">Global Partner</a></li>
              <li><a href="pages/client.php">Our Clients</a></li>
        <li><a href="pages/gallery.php">Gallery</a></li>
          <li><a href="news.php">Latest News</a></li>
          <li><a href="pages/contact.php">Contact Us</a></li>
         
        </ul>
      </nav>

    </div>
	
	
  </div>
</div>

  <div id="homeSlider" class="carousel slide" data-ride="carousel">
 
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100 img-fluid" src="images/demo/backgrounds/011.png" alt="First slide" style="height: 680px">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100 img-fluid" src="images/demo/backgrounds/5.png" alt="Second slide" style="height: 680px">
    </div>
    
    <div class="carousel-item">
      <img class="d-block w-100 img-fluid" src="images/demo/backgrounds/6.jpg" alt="Third slide" style="height: 680px">
    </div>

    <div class="carousel-item">
      <img class="d-block w-100 img-fluid" src="images/demo/backgrounds/dfgh.png" alt="Third slide" style="height: 680px">
    </div>
  </div>
  <a class="carousel-control-prev" href="#homeSlider" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#homeSlider" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  <!-- ################################################################################################ -->
  <!-- ### background: url(images/demo/4.jpg); ############################################################################################# -->


  <!-- ################################################################################################ -->
  <div class="wrapper1">
    <div id="pageintro" class="hoc clear"> 

      <div class="introtxt" style="border-style: solid; margin-left:-130px; margin-top: -480px;position: absolute; border-width: 1px; height: 330px; width:240px; padding: 10px;  text-align: justify; background-color: rgba(0,0,0, 0.4);  pad " >
        <h2 class="">Welcome to Fair-Bond Ltd.</h2><br>
        <p>Our MISSION is to facilitate and provide one stop supplies chemical raw material services to Paint, Ink, Adhesive, Textile, and Plastic , Safety Matches & Leather Industries & to equip manufacturers with competitive edge in domestic and in regional market.Our VISION is to -Create the sustainable long term business BOND with clients and suppliers.</p>
      </div>

      <div class="clear"></div>
    </div>
  </div>
  <!-- ################################################################################################ -->
<!-- End Top Background Image Wrapper -->
<!-- ################################################################################################ -->




<!-- 

  ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3" style="background-image:url('images/22.jpg');" >
  <div id="introblocks" class="hoc clear"> 
    <!-- ################################################################################################ -->

    <ul class="nospace group">
      <div id="d1">
      <li class="one_quarter first">
        <article><a class="btn" href="#"><i class="fa fa-paint-brush" aria-hidden="true"></i></a>
          <h4 class="heading underline center"><a href="#">Paints & Ink</a></h4>
        </article>
      </li>
    </div>

      <div id="d2">
      <li class="one_quarter">
        <article><a class="btn" href="#"><i class="fa fa-flask" aria-hidden="true"></i></a>
          <h4 class="heading underline center"><a href="#">Petrochemicals </a></h4>
        </article>
      </li>
      </div>

      <div id="d3">
      <li class="one_quarter">
        <article><a class="btn" href="#"><i class="fa fa-industry" aria-hidden="true"></i></a>
          <h4 class="heading underline center"><a href="#">Leather Processing </a></h4>
        </article>
      </li>
      </div>

      <div id="d4">
      <li class="one_quarter"> 
        <article><a class="btn" href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
          <h4 class="heading underline center"><a href="#">Textile Printing</a></h4>
        </article>
      </li>
      </div>
      
    </ul>
    <!-- ################################################################################################ -->
    <div class="clear"></div>
  </div>

<!-- ################################################################################################ -->
<!-- Welcome to fair bond limited -->
<div class="welcome"  style="background:linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url(images/demo/welcome.jpg); background-repeat: no-repeat;background-size: cover;  height: 250px; background-image: cover; "  >

  <h3 class="heading underline center" style="text-decoration: underline; opacity:1.0; color: white;padding-top:40px;" ><b>Welcome to Fair-Bond Ltd.</b></h3>

  <p style=" color: white; padding-left: 40px; opacity:1.0; padding-right: 40px; text-align: justify; /* For Edge */
  -moz-text-align-last: justify; /* For Firefox prior 58.0 */
  ">Established in the year 1997, is now one of the most reliable and capable chemical trading firm in chemical business community. The philosophy of customer satisfaction and long-term business enabled Fair-Bond to establish a solid business relationship with the leading suppliers and customers both in domestic and overseas. Based in Bangladesh providing quick and professional services backed by an experienced technical knowledge and extensive global network. Our VISION is to -Create the sustainable long term business BOND with clients and suppliers.   

Our MISSION is to facilitate and provide one stop supplies chemical raw material services to Paint, Ink, Adhesive, Textile, and Plastic , Safety Matches & Leather Industries & to equip manufacturers with competitive edge in domestic and in regional market.</p>
  


</div>
</div>


<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper row3" style="background-image:url('images/22.jpg');">
  <main class="hoc container clear"> 
    <h3 class="heading center" style="text-decoration: underline;" ><b>Our Products</h3>

    <!-- main body -->
    <!-- ################################################################################################ -->
    <ul id="home" class="nospace group elements">
      <li class="one_third first">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/pigment.php"><img src="New/ab/3.jpg" alt=""></a>
          
        </figure>
      </li>
      <li class="one_third">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/resins.php"><img src="New/ab/2.jpg" alt=""></a>
         
        </figure>
      </li>
      <li class="one_third">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/addi.php"><img src="New/filter/1.png" alt=""></a>
         
        </figure>
      </li>
    </ul>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>

<main class="hoc container clear" > 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <ul id="home" class="nospace group elements">
      <li class="one_third first">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/petro.php"><img src="New/petro/1.jpg" alt=""></a>
          
        </figure>
      </li>
      <li class="one_third">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/resins.php"><img src="New/petro/2.jpg" alt=""></a>
         
        </figure>
      </li>
      <li class="one_third">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/pigment.php"><img src="New/pigment/1.jpg" alt=""></a>
         
        </figure>
      </li>
    </ul>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>

 <main class="hoc container clear" > 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <ul id="home" class="nospace group elements">
      <li class="one_third first">
        <figure class="elementwrapper"><a class="imgoverlay" href="pages/filter.php"><img src="New/pigment/2.jpg" alt=""></a>
          
        </figure>
      </li>
      <li class="one_third">
        <figure class="elementwrapper"><a class="imgoverlay" href="#"><img src="New/Resins/1.jpg" alt=""></a>
         
        </figure>
      </li>
      <li class="one_third">
        <figure class="elementwrapper"><a class="imgoverlay" href="#"><img src="New/Resins/resin.jpg" alt=""></a>
         
        </figure>
      </li>
    </ul>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>



</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- <div class="wrapper row5" style="background:linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url(new/123.jpg); background-repeat: no-repeat;background-size:cover;  border-top: 1px solid black;
 " >
  <section id="holtypes" class="hoc clear" style="max-width: 90% "> 
   
    <h2 class="heading  center" style="text-decoration: underline; color:white; margin-top: 20px;">Our Supliers</h2>
    <ul class="nospace group row col-md-12">
      <li class="one_third first " style="max-width: 22% ;">
        <article><a class="btn" href="#"><img src="Fair-Bond Ltd._files/0.jpg" style=" border-radius: 50%; 
        margin-top: -120px; height: 90px; width: 100px;" alt=""></a>
          <h4 class="heading" style="text-decoration-color: red;">Hoyon</h4>
          
        </article>
      </li>
      <li class="one_third col-md-" style="max-width: 22% ;">
        <article><a class="btn" href="#"><img src="Fair-Bond Ltd._files/1.jpg" style=" border-radius: 50%; 
        margin-top: -120px; height: 90px; width: 100px;" alt=""></a>
          <h4 class="heading" style="margin-top: ;">Jessons</h4>
          
        </article>
      </li>
      <li class="one_third col-md-" style="max-width: 22% ;">
         <article><a class="btn" href="#"><img src="Fair-Bond Ltd._files/0a.jpg" style=" border-radius: 50%; 
        margin-top: -120px; height: 90px; width: 100px;" alt=""></a>
          <h4 class="heading" style="margin-top: ;">Italinto</h4>
          
        </article>
      </li>

      <li class="one_third col-md-" style="max-width: 22% ;">
         <article><a class="btn" href="#"><img src="Fair-Bond Ltd._files/0a.jpg" style=" border-radius: 50%; 
        margin-top: -120px; height: 90px; width: 100px;" alt=""></a>
          <h4 class="heading" style="margin-top: ;">Italinto</h4>
          
        </article>
      </li>

    </ul>
   
  </section>
</div>
 -->


<section id="features" class="blue" style="background:linear-gradient(rgba(0,0,0,0.4),rgba(0,0,0,0.4)), url(new/123.jpg); background-repeat: no-repeat;background-size:cover;  border-top: 1px solid black;
 height: 260px ">
      <div class="container">
        
      <h2 class="heading  center" style="text-decoration: underline; color:white; margin-top: 20px;margin-bottom: -1px;">Our Supliers</h2>
        <div class="slider responsive">
          
          <div>
            <img src="Fair-Bond Ltd._files/0.jpg">
            <h6 class="center" style=" color:white; margin-top:5px;">Hoyan</h6>

          </div>
          <div>
            <img src="Fair-Bond Ltd._files/1.jpg">
            <h6 class="center" style=" color:white; margin-top:5px;">Jesson</h6>
          </div>
     
          <div>
             <img src="Fair-Bond Ltd._files/0a.jpg">
             <h6 class="center" style=" color:white; margin-top:5px;">Italiano</h6>
          </div>
          <div>
              <img src="Fair-Bond Ltd._files/images-(1)a.jpg">
              <h6 class="center" style=" color:white; margin-top:5px;">Haydro</h6>
          </div>
          <div>
            <img src="Fair-Bond Ltd._files/images-(2).jpg">
            <h6 class="center" style=" color:white; margin-top:5px;">Hoyan</h6>
          </div>
          <div>
            <img src="Fair-Bond Ltd._files/images-(3).jpg">
            <h6 class="center" style=" color:white; margin-top:5px;">San Nopco</h6>
          </div>
          <div>
            <img src="Fair-Bond Ltd._files/images-(7).jpg">
            <h6 class="center" style=" color:white; margin-top:5px;">IPEL</h6>
          </div>
          <div>
            <img src="Fair-Bond Ltd._files/imagesaa.jpg">
            <h6 class="center" style=" color:white; margin-top:5px;">HD</h6>
          </div>
         
        </div>
      </div>
    </section>









<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- <div class="footerlast" style="background-image:url('images/demo/backgrounds/0202.png'); "> -->

 <div class="footerlast" style="background-image:url('images/footer.jpg'); ">
<img src="images/demo/logo2.png" style=" height: 90px; width: 100px; margin-top: 15px; margin-left: 620px;"  alt="">
 <h3 style="color:white; text-align: center; margin-top: 20px;">FAIR-BOND LIMITED</h3>

 <ul id="mainav" style="color:white; margin-left: 440px ">
  <li>ABOUT US</li>
  <li>PRODUCTS</li>
  <li>GALLERY</li>
  <li>CARRER</li>
  <li>CONTACT US </li>
</ul>

<div class="fl_right" style="margin-right: 570px; ">
        <ul class="faico clear">
          <li><a class="faicon-facebook fa-3x" href="#" style=" background-color: white;"><i class="fa fa-facebook"></i></a></li>
          <li><a class="faicon-pinterest" href="#" style=" background-color: white;"><i class="fa fa-pinterest"></i></a></li>
          <li><a class="faicon-twitter" href="#" style=" background-color: white;"><i class="fa fa-twitter"></i></a></li>
          <li><a class="faicon-dribble" href="#" style=" background-color: white;"><i class="fa fa-dribbble"></i></a></li>
          <li><a class="faicon-linkedin" href="#" style=" background-color: white;"><i class="fa fa-linkedin"></i></a></li>
          <li><a class="faicon-google-plus" href="#" style=" background-color: white;"><i class="fa fa-google-plus"></i></a></li>
          <li><a class="faicon-rss" href="#" style=" background-color: white;"><i class="fa fa-rss"></i></a></li>
        </ul>
      </div>

      <!-- <div class="inline">
      <h1 style="text-align: center; color: white; margin-top: 70px; font-size: 16px;" >H Trade BD © 2019, Designed & Developed by</h1>
      <img src="images/base.png" style=" height: 40px; width:40px;" alt="">
      </div> -->

      <div class="inline" style="">
       <h1 style=" color: white; margin-top: 20px; font-size: 16px; margin-left: 476px;" >Fair-Bond Limited  © 2019, Designed & Developed by</h1>
       <a href="https://baseit.com.bd"> <img src="images/base.png" style="height: 30px; width: 40px; margin-top:-7px;margin-left:16px;" alt=""></a>

        </div>

       <!--  <div class="site-info">
            H Trade BD © 2019, Designed & Developed by <a href="https://baseit.com.bd">
              <img  src="images/base.png" alt="Base IT" width="35px;" height="20px;"></a> -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ 
<!-- ################################################################################################ 
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
<!-- JAVASCRIPTS -->
<!--<script src="layout/scripts/jquery.min.js"></script>-->
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
<!-- IE9 Placeholder Support -->
<script src="../layout/scripts/jquery.placeholder.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>

<!-- / IE9 Placeholder Support -->
<!-- Homepage specific - can be used in all pages -->
<!--<div id="preloader"><div></div></div>
<script>$(window).load(function(){$("#preloader div").delay(500).fadeOut();$("#preloader").delay(800).fadeOut("slow");});</script>-->
<!-- / Homepage specific -->
<script >
 $(document).ready(function() {

    $('.responsive').slick({
        // dots: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        autoplaySpeed: 2000,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1,
                // centerMode: true,

            }

        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2,
                dots: true,
                infinite: true,

            }


        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true,
                infinite: true,
                
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 2000,
            }
        }]
    });


});

 var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}



</script>

</body>
</html>