-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2019 at 04:12 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlogin`
--

CREATE TABLE `adminlogin` (
  `id` int(10) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminlogin`
--

INSERT INTO `adminlogin` (`id`, `userName`, `password`) VALUES
(1, 'admin', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `adminnotice`
--

CREATE TABLE `adminnotice` (
  `id` int(15) NOT NULL,
  `content` varchar(500) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminnotice`
--

INSERT INTO `adminnotice` (`id`, `content`, `date`) VALUES
(1, 'Holiday gg all', '2017-05-02'),
(3, 'ha ahd adha dh ad ll', '0001-02-10'),
(6, 'Tomorrow Office will be closed for Ramadan. After 3days Vacation office will be start, So Everybody Join Just Time. \r\nOrdered By\r\nMd.Saddam Hososain    ', '5/31/17');

-- --------------------------------------------------------

--
-- Table structure for table `empattendance`
--

CREATE TABLE `empattendance` (
  `id` int(20) NOT NULL,
  `empId` int(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `inTime` varchar(20) NOT NULL,
  `early` varchar(20) NOT NULL,
  `late` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL,
  `statusCount` varchar(20) NOT NULL,
  `month` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empattendance`
--

INSERT INTO `empattendance` (`id`, `empId`, `date`, `inTime`, `early`, `late`, `status`, `statusCount`, `month`) VALUES
(1, 1, '12/06/17', '08:24:07pm', '', '10:54:07', 'Present', '', 'June'),
(2, 2, '12/06/17', '08:41:05pm', '', '11:11:05', 'Present', '', 'June'),
(4, 3, '12/06/17', '08:43:42pm', '', '11:13:42', 'Present', '', 'June'),
(5, 5, '15/06/17', '12:25:22am', '09:04:38', '', 'Present', '', 'June'),
(6, 1, '15/06/17', '', '', '', 'Absent', '1', 'June'),
(7, 2, '15/06/17', '', '', '', 'Absent', '1', 'June'),
(8, 2, '25/11/19', '05:14:09pm', '', '07:44:09', 'Present', '', 'November');

-- --------------------------------------------------------

--
-- Table structure for table `employeeprofile`
--

CREATE TABLE `employeeprofile` (
  `id` int(20) NOT NULL,
  `empName` varchar(200) NOT NULL,
  `empGender` varchar(10) NOT NULL,
  `empEmail` varchar(200) NOT NULL,
  `empMobile` varchar(15) NOT NULL,
  `empEdu` varchar(200) NOT NULL,
  `empDept` varchar(100) NOT NULL,
  `empPosition` varchar(100) NOT NULL,
  `empSalary` varchar(20) NOT NULL,
  `empBloodGrpup` varchar(10) NOT NULL,
  `empNid` varchar(20) NOT NULL,
  `empBirthDate` varchar(20) NOT NULL,
  `empJoiningDate` varchar(20) NOT NULL,
  `empImage` varchar(200) NOT NULL,
  `created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeeprofile`
--

INSERT INTO `employeeprofile` (`id`, `empName`, `empGender`, `empEmail`, `empMobile`, `empEdu`, `empDept`, `empPosition`, `empSalary`, `empBloodGrpup`, `empNid`, `empBirthDate`, `empJoiningDate`, `empImage`, `created`) VALUES
(1, 'Md. Kawsar Ahamad', 'Male', 'mdkawsar1653@gmail.com', '01728019740', 'BSc in CSE', 'IT', 'Sr. IT Officer', '30000', 'A+', '111555666999', '1/1/1997', '1/1/17', '../pImage/kawsar.jpg', '0000-00-00 00:00:00.000000'),
(2, 'Emran Hossain', 'Male', 'emran@gmail.com', '01760326414', 'BSc in CSE', 'IT', 'MIS Officer', '25000', 'A+', '111555666999', '1/1/1997', '1/1/17', '../pImage/Screenshot_1.png', '0000-00-00 00:00:00.000000'),
(3, 'Foysal Ahmad', 'Male', 'foysal@gmail.com', '01682786501', 'BSc in CSE', 'Software', 'Team Leader', '60000', 'A+', '111555666999', '1/1/1995', '1/1/17', '../pImage/cto.jpg', '0000-00-00 00:00:00.000000'),
(4, 'Najmul islam', 'Male', 'najmul@yahoo.com', '01682786501', 'BSc in CSE', 'Software', 'Sr. Software Engineer', '45000', 'A+', '111555666999', '1/1/1995', '1/1/17', '../pImage/sr.jpg', '0000-00-00 00:00:00.000000'),
(5, 'Md.Saddam Hossain', 'Male', 'sunny21khan@gmail.com', '01680069682', 'BSc in CSE', 'IT', 'CTO', '50000', 'A+', '111555666999', '20/3/1992', '1/1/17', '../pImage/avatar.png', '0000-00-00 00:00:00.000000'),
(6, 'Din Islam', 'Male', 'din@yahoo.com', '01728019740', 'HSC', 'IT', 'jr. It Officer', '25000', 'A+', '111555666999', '1/1/1998', '1/1/17', '../pImage/images.jpg', '0000-00-00 00:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `empsalary`
--

CREATE TABLE `empsalary` (
  `id` int(20) NOT NULL,
  `officeId` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL,
  `salaryMonth` varchar(20) NOT NULL,
  `totalabsent` varchar(20) NOT NULL,
  `salary` varchar(20) NOT NULL,
  `lessSalary` varchar(20) NOT NULL,
  `totalsalary` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empsalary`
--

INSERT INTO `empsalary` (`id`, `officeId`, `date`, `salaryMonth`, `totalabsent`, `salary`, `lessSalary`, `totalsalary`) VALUES
(0, '3', '25/11/19', 'November', '', '60000', '0.00', '60,000.00'),
(1, '1', '12/06/17', 'June', '0', '30000', '0.00', '30,000.00'),
(2, '5', '13/06/17', 'June', '', '50000', '0.00', '50,000.00'),
(3, '2', '15/06/17', 'June', '1', '25000', '833.33', '24,166.67');

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE `userlogin` (
  `id` int(20) NOT NULL,
  `officeId` varchar(20) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`id`, `officeId`, `userName`, `password`, `created`) VALUES
(1, '5', 'sunny', '12345', '2017-06-08 23:31:50.141393'),
(2, '2', 'imran', '12345', '2017-06-13 19:28:02.779835'),
(5, '5918', 'Foysal', '12345', '0000-00-00 00:00:00.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogin`
--
ALTER TABLE `adminlogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adminnotice`
--
ALTER TABLE `adminnotice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empattendance`
--
ALTER TABLE `empattendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empsalary`
--
ALTER TABLE `empsalary`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogin`
--
ALTER TABLE `adminlogin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `adminnotice`
--
ALTER TABLE `adminnotice`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `empattendance`
--
ALTER TABLE `empattendance`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
