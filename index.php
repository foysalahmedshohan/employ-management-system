
<!DOCTYPE html>
    <html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DIU</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="style/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="style/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="style/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<style>
    body{
        background-image: url("image/index.jpg");
        background-repeat: no-repeat;
        background-size: cover;
    }
    .loginH2{
        font-size: 60px;
        color:white;
        margin-bottom:70px;

    }
    .marginTop{margin-top:90px;}
    .adminBtn,.userBtn{
        font-size:35px;
        border-radius: 15px;
        color:wheat;
    }
</style>
</head>
<body>
<div class="container">
    <div class="row text-center ">
        <div class="col-md-12">
            <br /><br />
            <h2 class=" loginH2">Employee Management System</h2>
            <br />
        </div>
    </div>
    <div class="row text-center marginTop ">

            <div class="col-md-12">

                <a href="login/adminLogin.php"><button class="btn btn-primary adminBtn" name="adminBtn">ADMIN LOGIN</button></a>
            </div>
    </div><br>
        <div class="row text-center ">

            <div class="col-md-12">

                <a href="login/userLogin.php"><button class="btn btn-primary userBtn" name="userBtn">USER LOGIN</button></a>
            </div>
        </div>
</div>


<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="style/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="style/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="style/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="style/js/custom.js"></script>

</body>
</html>
