
<?php
include ("../include/header.php");
include("../login/session.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css">
</head>

<body><br/>
<br/>
<br/>
<br/>
<br/>
<div class="row ">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ">
<?php
$officeId=$_POST['employeeId'];
$selectMonth=$_POST['selectmonth'];
date_default_timezone_set('Asia/Dhaka');
$date= date("d/m/y");
$month=date("F");



$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `employeeprofile` WHERE employeeprofile.id=$officeId";
$stmt = $db->query($query);
$empAll = $stmt->fetch(PDO::FETCH_ASSOC);


$query2="SELECT empId, SUM(statusCount) total FROM empattendance WHERE empattendance.empId=$officeId and empattendance.month='$month'";
$stmt2 = $db->query($query2);
$getabsent = $stmt2->fetch(PDO::FETCH_ASSOC);


$perDaySalary= ($empAll['empSalary']/$selectMonth);
$absentAmount=$perDaySalary*$getabsent['total'];
$showAbAmount=number_format($absentAmount,2);
$getMonthlySalary= $empAll['empSalary']-$absentAmount;
$resultsalary=number_format($getMonthlySalary,2);


$query3= "SELECT * FROM `empsalary`";
$stmt3 = $db->query($query3);
$salaryALl = $stmt3->fetchAll(PDO::FETCH_ASSOC);


foreach ($salaryALl as $all) {
    if ($officeId == $all['officeId'] && $date = $all['date']) {
        echo "<h1 style='color:red; text-align:center; margin-top:100px;'>Failed  !! This Employees Salary Calculated</h1> ";


        echo "<a href='../salary/calculateSalary.php'><button class='btn btn-danger' style='margin-left: 180px;'>Go Back</button>   </a>";
        die();
    }}

$query4="INSERT INTO `empsalary` (`officeId`,`date`,`salaryMonth`,`totalabsent`,`salary`,`lessSalary`,`totalsalary`) VALUES ('" . $officeId . "','".$date."', '".$month."', '".$getabsent['total']."', '".$empAll['empSalary']."', '".$showAbAmount."', '".$resultsalary."');";
$result4 = $db->exec($query4);


if ($result4) {
    header("location:../salary/salaryReport.php");
    echo "Data has been saved sucessfully.";
} else {
   echo "There is an error. Please try again later.";

}

?>
    </div></div></body></html>
