<?php
include ("../include/header.php");
include("../login/session.php");

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$attQuery="SELECT employeeprofile.id,employeeprofile.empName, employeeprofile.empDept, employeeprofile.empPosition, empsalary.officeId, empsalary.date, empsalary.salaryMonth,empsalary.totalabsent, empsalary.salary, empsalary.lessSalary, empsalary.totalsalary FROM employeeprofile INNER JOIN empsalary ON employeeprofile.id = empsalary.officeId  ORDER BY empsalary.date  DESC";
$stmt = $db->query($attQuery);
$allReportSalary = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/attenReport.jpeg");
    .empList{color:darkred;}
</style>
<body>

<div class="container addEmployee">
    <div class="row">
        <div class="col-md-offset-10 col-md-4">
             <a href="../pdf/downloadpdf.php" target="_new"><button class="btn btn-danger">View pdf</button></a>
    </div>

    </div>
    <div class="row">
        <div class=" col-md-12"><br>
            <table class="table table-bordered" style="text-align:center;">
                <thead style="color:white;">
                <tr>
                    <th style="text-align:center;">Sl. No.</th>
                    <th style="text-align:center;">Employee ID</th>
                    <th style="text-align:center;">Date</th>
                    <th style="text-align:center;">Month Name</th>
                    <th style="text-align:center;">Name</th>
                    <th style="text-align:center;">Department</th>
                    <th style="text-align:center;">Position</th>
                    <th style="text-align:center;">Total Absent</th>
                    <th style="text-align:center;">Salary </th>
                    <th style="text-align:center;">Les Absent</th>
                    <th style="text-align:center;">Total Salary</th>

                </tr>
                </thead>
                <tbody>
<?php
$counter = 1;
foreach($allReportSalary as $salaryReport):

    ?>
                <tr>
                    <td><?php echo $counter++;?></td>
                    <td><?php echo $salaryReport['id']?></td>
                    <td><?php echo $salaryReport['date']?></td>
                    <td><?php echo $salaryReport['salaryMonth']?></td>
                    <td><?php echo $salaryReport['empName']?></td>
                    <td><?php echo $salaryReport['empDept']?></td>
                    <td><?php echo $salaryReport['empPosition']?></td>
                    <td><?php echo $salaryReport['totalabsent']?></td>
                    <td><?php echo $salaryReport['salary']?></td>
                    <td><?php echo $salaryReport['lessSalary']?></td>
                    <td><?php echo $salaryReport['totalsalary']?></td>
                </tr>
    <?php
endforeach;
?>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>