<?php
include ("../include/header.php");
include("../login/session.php");

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `employeeprofile` WHERE id=".$_GET['id'];
//execution
$stmt = $db->query($query);
$empProfile = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en"
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/addEmployee.jpg");

</style>
<body>

<div class="container addEmployee">

    <h2>Edit Employees Information</h2>
    <img src="<?=$empProfile['empImage']?>" class="img-circle"/>
    <p>Office ID: <span><?=$empProfile['id']?></span></p>
    <p>Employee Name:<span><?=$empProfile['empName']?></span></p>
    <p>Department:<span><?=$empProfile['empDept']?></span></p>
    <p>Position:<span><?=$empProfile['empPosition']?></span></p>
    <form action="empSalaryUpdate.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?=$_GET['id']?>"/>
        <div class="row">
            <div class="col-md-4  addEmployee">
                <div class="form-group">
                    <label for="empSalary">Salary:</label>
                    <input type="text" class="form-control addEmployee" id="empSalary" value="<?=$empProfile['empSalary']?>" name="empSalary">
                </div>
                <div class="form-inline">
                    <button type="submit" class="btn btn-success"onclick="return confirm('Are you sure Update this Information?');">Submit</button>
                </div>
            </div>
    </form>
</div>
</div>

</body>
</html>
