<?php
include ("../include/header.php");
include("../login/session.php");

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `employeeprofile`";
$stmt = $db->query($query);
$empAll = $stmt->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/getAttendance.jpg");

</style>
<body>

<div class="container addEmployee ">
<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
    <h2>Select Employee ID </h2>
    <form class="form-group" action="../activity/profileView.php" method="post">
        <div class="form-group">
            <label for="employeeId">Enter Employee Id:</label>
            <select name="employeeId" class="form-control" id="employeeId" >
                <option value="">Select an Employee</option>
                <?php
                foreach ($empAll as $empIds):

                    ?>
                    <option value="<?=$empIds['id']?>"> <?= $empIds['id']."  "."Name:". $empIds['empName']?> </option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</div>

</body>
</html>