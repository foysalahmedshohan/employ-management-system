<?php
include ("../include/header.php");
include("../login/session.php");
$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `employeeprofile`";
$stmt = $db->query($query);
$empAll = $stmt->fetchAll(PDO::FETCH_ASSOC);



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/employeeList.jpg");
.empList{color:darkred;}
</style>
<body>

<div class="container addEmployee">
<div class="row">
    <div class=" col-md-12">
        <table class="table table-bordered ">
            <thead>
            <tr>
                <th>Sl. No.</th>
                <th>Employee ID</th>
                <th>Name</th>
                <th>Department</th>
                <th>Position</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>


<?php
$counter = 1;
foreach($empAll as $empList):

?>

                <tr>
                    <td><?php echo $counter++;?></td>
                    <td><?php echo $empList['id']?></td>
                    <td><?php echo $empList['empName']?></td>
                    <td><?php echo $empList['empDept']?></td>
                    <td><?php echo $empList['empPosition']?></td>

                    <td>
                        <a  href="empShow.php?id=<?php echo $empList['id']?>">Show</a> |
                        <a href="empEdit.php?id=<?php echo $empList['id']?>">Edit</a> |
                        <a href="empDelete.php?id=<?php echo $empList['id']?>" onclick="return confirm('Are you sure Delete this Information?');">Delete</a>
                        <!--<a href="">Edit Courses</a>-->
                    </td>
                </tr>

    <?php
endforeach;
?>
            </tbody>
        </table>
    </div>
    </div>

    </body>
    </html>