<?php
include ("../include/header.php");
include("../login/session.php");
$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `adminNotice`";
$stmt = $db->query($query);
$aNotice = $stmt->fetchAll(PDO::FETCH_ASSOC);




?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/employeeList.jpg");
    .empList{color:darkred;}
</style>
<body>

<div class="container addEmployee">
    <div class="row">
        <div class=" col-md-12">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Sl. No.</th>
                    <th>id</th>
                    <th>Employee ID</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>


                <?php
                $counter = 1;
                foreach($aNotice as $notice):

                    ?>

                    <tr>
                        <td><?php echo $counter++;?></td>
                        <td><?php echo $notice['id']?></td>
                        <td><?php echo $notice['content']?></td>
                        <td>
                            <a  href="showNotice.php?id=<?php echo $notice['id']?>">Show</a> |
                            <a href="editNotice.php?id=<?php echo $notice['id']?>">Edit</a> |
                            <a href="deleteNotice.php?id=<?php echo $notice['id']?>" onclick="return confirm('Are you sure Delete this Information?');">Delete</a>
                            <!--<a href="">Edit Courses</a>-->
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>