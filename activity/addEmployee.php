<?php
include ("../include/header.php");
include("../login/session.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>DIU</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />

</head>
<style>body{  background-image: url("../image/addEmployee.jpg");

</style>
<body>

<div class="container addEmployee">

  <h2>Add New Employee</h2>

        <form action="empStore.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-4  addEmployee">
        <div class="form-group">
      <label for="empName">Employee Name:</label>
      <input type="text" class="form-control addEmployee" id="empName" placeholder="Enter Employee Name" name="empName" required>
    </div>

                    <div class="form-group">
                        <label for="Egender">Gender:</label><br>
                        <label class="radio-inline"><input type="radio" name="Egender" value="Female">Female</label>
                        <label class="radio-inline"><input type="radio" name="Egender" value="Male">Male</label>
                        <label class="radio-inline"><input type="radio" name="Egender" value="Others">Others</label>
                    </div>
                    <div class="form-group">
                        <label for="bDate">Birth Date:</label>
                        <input type="date" class="form-control addEmployee" id="datepicker"  placeholder="Enter Employee Birth Date" name="bDate" required>
                    </div>
                    <div class="form-group">
                        <label for="eBlodG">Blood Group:</label>
                        <input type="text" class="form-control addEmployee" id="eBlodG" placeholder="Enter Employee Blood Group" name="eBlodG" required>
                    </div>
                    <div class="form-group">
                        <label for="eNid">NID Number:</label>
                        <input type="text" class="form-control addEmployee" id="eNid" placeholder="Enter Employee NID No" name="eNid" required>
                    </div>
                </div>

            <div class="col-md-4  addEmployee">
                <div class="form-group">
                    <label for="topEdu">Top Education:</label>
                    <input type="text" class="form-control addEmployee" id="topEdu" placeholder="Enter Top Education" name="topEdu" required>
                </div>
      <div class="form-group">
          <label for="deptName">Department:</label>
          <input type="text" class="form-control addEmployee" id="deptName" placeholder="Enter Department Name" name="deptName" required>
</div>
      <div class="form-group">
          <label for="positionName">Position:</label>
          <input type="text" class="form-control addEmployee" id="positionName" placeholder="Enter Position Name" name="positionName" required>
      </div>
      <div class="form-group">
          <label for="positionName">Salary:</label>
          <input type="number" class="form-control addEmployee" id="positionName"  name="empSalary" required>
      </div>

            </div>

                <div class="col-md-4 addEmployee">




      <div class="form-group">
          <label for="eEmail">Email:</label>
          <input type="email" class="form-control addEmployee" id="eEmail" placeholder="Enter Employee Mail" name="eEmail" required>
      </div>
      <div class="form-group">
          <label for="eMobile">Mobile:</label>
          <input type="tel" class="form-control addEmployee" id="eMobile" placeholder="Enter Employee Mobile" name="eMobile" required>
      </div>
      <div class="form-group">
          <label for="employeeImage">input Image</label>
          <input type="file" class="form-control-file" id="employeeImage" name="employeeImage" aria-describedby="fileHelp" required>
      </div>
                    <div class="form-group">
                        <label for="joinDate">Joining Date:</label>
                        <input type="date" class="form-control addEmployee" id="datepicker2"  placeholder="Enter Employee Joining Date" name="joinDate" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
                        <div class="form-inline">
                            <input type="reset" class="btn btn-danger" name="reset" value="Reset">
                            <button type="submit" class="btn btn-success" onclick="return confirm('Are you sure submit this Information?');">Submit</button>
                        </div>
                    </div>
                </div>
        </form>
</div>



</body>
</html>





