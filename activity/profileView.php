<?php
include ("../include/header.php");
include("../login/session.php");

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `employeeprofile` WHERE id=".$_POST['employeeId'];
//execution
$stmt = $db->query($query);
$empProfile = $stmt->fetch(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/getAttendance.jpg");


</style>
<body>

<div class="container addEmployee ">
    <div class="row">
        <div class="col-sm-4  addEmployee">
            <img src="<?=$empProfile['empImage']?>" class="img-circle"/>
        </div>
        <div class="col-sm-8 addEmployee">


            <p>Office ID: <span><?=$empProfile['id']?></span></p>

            <p>Employee Name:<span><?=$empProfile['empName']?></span></p>

            <p>Department:<span><?=$empProfile['empDept']?></span></p>

            <p>Position:<span><?=$empProfile['empPosition']?></span></p>
            <p>Salary:<span><?=$empProfile['empSalary']?></span></p>
            <p>Education: <span><?=$empProfile['empEdu']?></span></p>
            <p>Date of birth:<span><?=$empProfile['empBirthDate']?></span></p>
            <p>Blood group:<span><?=$empProfile['empBloodGrpup']?></span></p>
            <p>Email:<span><?=$empProfile['empEmail']?></span></p>
            <p>Mobile:<span><?=$empProfile['empMobile']?></span></p>
            <p>Joining Date:<span><?=$empProfile['empJoiningDate']?></span></p>

        </div>
    </div>

</body>
</html>