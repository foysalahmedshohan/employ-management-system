<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css">
</head>

<body><br/>
<br/>
<br/>
<br/>
<br/>
<div class="row ">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 ">
<?php
include("../login/session.php");
$officeId=$_POST['employeeId'];
$stReport=$_POST['status'];
date_default_timezone_set('Asia/Dhaka');
$inputdate= date("d/m/y");
$month=date("F");
$inTime= date("h:i:sa");

$officeTime=date("09:30");
//convert time for calculate;
$convertIntime=strtotime("$inTime");
$convertOfficeTime=strtotime("$officeTime");
$earlyTIme='';
$lateTime='';

//Get early and late calculation;
if($convertIntime<=$convertOfficeTime){
    $calEarly=($convertOfficeTime-$convertIntime);
    $earlyTIme=gmdate("H:i:s", $calEarly);
}
else{
    $calLate=($convertIntime-$convertOfficeTime);
    $lateTime=gmdate("H:i:s", $calLate);
}

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$attQuery="SELECT * FROM `empattendance`";
$stmt = $db->query($attQuery);
$attStatus = $stmt->fetchAll(PDO::FETCH_ASSOC);

$query="SELECT * FROM `empattendance` WHERE empattendance.empId=$officeId and empattendance.date='$inputdate' ";
$stmt = $db->query($query);
$empAll = $stmt->fetch(PDO::FETCH_ASSOC);


    if  ( $inputdate == $empAll['date']){
        echo "<h1 style='color:red; text-align:center; margin-top:100px;'>Failed  !! This Id Already Use Today</h1> ";


        echo "<a href='../activity/getAttendance.php'><button class='btn btn-danger' style='margin-left: 180px;'>Go Back</button>   </a>";
        die();


}


    if ($stReport =="Present") {
        $query = "INSERT INTO `empattendance` (`empId`, `date`, `inTime`, `early`, `late`,`month`, `status`) VALUES ('" . $officeId . "', '" . $inputdate . "', '" . $inTime . "', '" . $earlyTIme . "','" . $lateTime . "','" . $month . "','" . $stReport . "');";
        $result = $db->exec($query);
        if ($result) {
            header("location:attendanceReport.php");
            echo "Data has been saved sucessfully.";
        } else {
            echo "There is an error. Please try again later.";

        }
    }
    elseif($stReport =="Absent")
    {
         $countstatus=1;
        $query = "INSERT INTO `empattendance` (`empId`,`date`,`status`,`month`,`statusCount`) VALUES ('".$officeId."', '".$inputdate."','".$stReport."','".$month."','".$countstatus."');";
        $result = $db->exec($query);
        if ($result) {
            header("location:attendanceReport.php");
            echo "Data has been saved sucessfully.";
        } else {
            echo "There is an error. Please try again later.";

        }
    }


?>
    </div>
</div>
</body>
</html>
