<?php
include ("../include/header.php");
include("../login/session.php");

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `employeeprofile` WHERE id=".$_GET['id'];
//execution
$stmt = $db->query($query);
$empProfile = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en"
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/addEmployee.jpg");

</style>
<body>

<div class="container addEmployee">

    <h2>Edit Employees Information</h2>
    <img src="<?=$empProfile['empImage']?>" class="img-circle"/>
    <form action="empUpdate.php" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?=$_GET['id']?>"/>
        <div class="row">
            <div class="col-md-4  addEmployee">
                <div class="form-group">
                    <label for="empName">Employee Name:</label>
                    <input type="text" class="form-control addEmployee" id="empName" value="<?=$empProfile['empName']?>" name="empName">
                </div>
                <div class="form-group">
                    <label for="bDate">Birth Date:</label>
                    <input type="date" class="form-control addEmployee" id="bDate"  value="<?=$empProfile['empBirthDate']?>" name="empBirthDate">
                </div>
                <div class="form-group">
                    <label for="eBlodG">Blood Group:</label>
                    <input type="text" class="form-control addEmployee" id="eBlodG" value="<?=$empProfile['empBloodGrpup']?>" name="empBloodGrpup">
                </div>
                <div class="form-group">
                    <label for="eNid">NID Number:</label>
                    <input type="text" class="form-control addEmployee" id="eNid" value="<?=$empProfile['empNid']?>" name="empNid">
                </div>
            </div>

            <div class="col-md-4  addEmployee">
                <div class="form-group">
                    <label for="deptName">Department:</label>
                    <input type="text" class="form-control addEmployee" id="deptName" value="<?=$empProfile['empDept']?>" name="deptName">
                </div>
                <div class="form-group">
                    <label for="positionName">Position:</label>
                    <input type="text" class="form-control addEmployee" id="positionName" value="<?=$empProfile['empPosition']?>" name="positionName">
                </div>
                <div class="form-group">
                    <label for="eEmail">Email:</label>
                    <input type="email" class="form-control addEmployee" id="eEmail" value="<?=$empProfile['empEmail']?>" name="eEmail">
                </div>
                <div class="form-group">
                    <label for="eMobile">Mobile:</label>
                    <input type="tel" class="form-control addEmployee" id="eMobile" value="<?=$empProfile['empMobile']?>" name="eMobile">
                </div>
            </div>

            <div class="col-md-4 addEmployee">

                <div class="form-group">
                    <label for="employeeImage">input Image</label>
                    <input type="file" class="form-control-file" id="employeeImage" name="employeeImage" aria-describedby="fileHelp">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 addEmployee">
                    <div class="form-inline">
                        <button type="submit" class="btn btn-success" onclick="return confirm('Are you sure Update this Information?');">Submit</button>
                    </div>
                </div>
            </div>
    </form>
</div>
</div>

</body>
</html>
