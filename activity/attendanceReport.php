<?php
include ("../include/header.php");
include("../login/session.php");


$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$attQuery="SELECT employeeprofile.empName, employeeprofile.empDept, employeeprofile.empPosition, empattendance.empId, empattendance.date, empattendance.inTime,empattendance.month, empattendance.early, empattendance.late, empattendance.status FROM 
employeeprofile INNER JOIN empattendance ON employeeprofile.id = empattendance.empId  ORDER BY empattendance.date  DESC";
$stmt = $db->query($attQuery);
$attStatus = $stmt->fetchAll(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/attenReport.jpeg");
    .empList{color:darkred;}
</style>
<body>

<div class="container addEmployee">
    <div class="row">
        <div class=" col-md-12">
            <table class="table table-bordered ">
                <thead style="color:white;">
                <tr>
                    <th>Sl. No.</th>
                    <th>Date</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Position</th>
                    <th>In Time</th>
                    <th>Early </th>
                    <th>Late</th>
                    <th>Status</th>
                    <th>Month</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $counter = 1;
                foreach($attStatus as $showStatus):
                    //for controll color;
if($showStatus['status']=='Present'){
    $fontcolor="green";
}else{$fontcolor="red";}
                ?>
                <tr>
                    <td><?php echo $counter++;?></td>
                    <td><?php echo $showStatus['date']?></td>
                    <td><?php echo $showStatus['empId']?></td>
                    <td><?php echo $showStatus['empName']?></td>
                    <td><?php echo $showStatus['empDept']?></td>
                    <td><?php echo $showStatus['empPosition']?></td>
                    <td><?php echo $showStatus['inTime']?></td>
                    <td><?php echo $showStatus['early']?></td>
                    <td><?php echo $showStatus['late']?></td>
                    <td style="color:<?=$fontcolor?>;"><?php echo $showStatus['status']?></td>
                    <td> <?php echo $showStatus['month']?></td>
                </tr>
                    <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>