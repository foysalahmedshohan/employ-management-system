<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <link rel="icon" type="image/png" href="../style/image/mLogo.jpg" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../style/css/jquery.min.js"></script>
    <script src="../style/css/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../dash/dashboard.php">Infinity Technology  BD</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Activity <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../activity/addEmployee.php">Add New Employee</a></li>
                        <li><a href="../activity/getAttendance.php">Get Attendance</a></li>
                        <li><a href="../activity/notice.php">Notice</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Employee Salary <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../salary/epmPresentSalary.php">Employees Present Salary</a></li>
                        <li><a href="../salary/calculateSalary.php">Calculate Monthly Salary</a></li>
                        <li><a href="../salary/salaryReport.php">Monthly Salary Report</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Status View <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="../activity/employeeList.php">Employee List</a></li>
                        <li><a href="../activity/employeeProfile.php">Employee Profile</a></li>
                        <li><a href="../activity/attendanceReport.php">Attendance Report</a></li>
                        <li><a href="../activity/noticeView.php">Notice View</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../login/logOut.php"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>

        </div>
    </div>
</nav>

