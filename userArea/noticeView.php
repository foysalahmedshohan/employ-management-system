<?php

include("../userArea/userSession.php");
include("../userArea/userHeader.php");

$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `adminnotice` ORDER BY adminnotice.date DESC";
//execution
$stmt = $db->query($query);
$aNotice = $stmt->fetchall(PDO::FETCH_ASSOC);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>EMS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../style/css/bootstrap.min.css"><link href="../style/css/login.css" rel="stylesheet" />
</head>
<style>body{  background-image: url("../image/employeeList.jpg");
    .empList{color:darkred;}
</style>
<body>

<div class="container addEmployee">
    <div class="row">
        <div class=" col-md-12">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>Sl. No.</th>
                    <th>Date</th>
                    <th>Notice</th>

                </tr>
                </thead>
                <tbody>


                <?php
                $counter = 1;
                foreach($aNotice as $notice):

                    ?>

                    <tr>
                        <td><?php echo $counter++;?></td>
                        <td><?php echo $notice['date']?></td>
                        <td><?php echo $notice['content']?></td>

                    </tr>
                    <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>
