



<?php

include("../userArea/userSession.php");
$check3=$_SESSION['officeId'];
$db = new PDO('mysql:host=localhost;dbname=ems;charset=utf8mb4', 'root', '');
$query="SELECT * FROM `employeeprofile` WHERE  employeeprofile.id=$check3";
$stmt = $db->query($query);
$empAll = $stmt->fetch(PDO::FETCH_ASSOC);




?>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>EMS</title>
    <link rel="icon" type="image/png" href="../style/image/mLogo.jpg" />
    <!-- BOOTSTRAP STYLES-->
    <link href="../style/css/bootstrap.min.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../style/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <!-- CUSTOM STYLES-->
    <link href="../style/css/custom.css" rel="stylesheet" />
    <link href="../style/css/login.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <style>body{background-color: unset;
            background-image: none;}</style>
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="">User Profile</a>
        </div>
        <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">  <a href="../login/logOut.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="<?=$empAll['empImage']?>" class="user-image img-responsive"/>
                </li>


                <li>
                    <a class="active-menu"  href=""><i class="fa fa-3x"></i> <?=$empAll['empName']?></a>
                </li>
              <!--  <li>
                    <a href=""><i class="fa fa-inbox fa-3x"></i> Inbox (01)
                </li>
                <li>
                    <a href=""><i class="fa fa-home fa-3x"></i> Home
                </li>-->

                <li>
                    <a href="#"><i class="fa fa-sitemap fa-3x"></i> Activity
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../userArea/myProfile.php">My Profile</a>
                            </li>
                            <li>
                                <a href="../userArea/userPost.php">Post</a>
                            </li>
                            <li>
                                <a href="../userArea/noticeView.php">Notice</a>
                            </li>
                        </ul>


        </div>

    </nav>
    <!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-weight:bold;">Infinity Technology BD</h2>
                    <h5 style="color:white;">Welcome Infinity Technology world. </h5>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-envelope-o"></i>
                </span>
                        <div class="text-box" >
                            <p class="main-text">My Profile</p>
                            <p class="text-muted">profile view</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-bars"></i>
                </span>
                        <a href="../activity/notice.php" class="dashA">
                            <div class="text-box dashA" >
                                <p class="main-text">Post</p>
                                <p class="text-muted">post to admin</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-envelope-o"></i>
                </span>
                        <a href="../activity/attendanceReport.php" class="dashA">
                            <div class="text-box">
                                <p class="main-text">Notice</p><br>
                                <p class="text-muted">Notice view</p>
                            </div></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-rocket"></i>
                </span>
                        <div class="text-box" >
                            <p class="main-text">Salary Report</p>
                            <p class="text-muted">Total information status</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />




</body>
</html>
